/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.DTOs;

import java.util.Collection;
import javafx.print.Collation;

/**
 *
 * @author RENT
 */
public class BookDTO {
    
    private Integer id;
    private String name;
    private int isbn;
    private int year;
    private Collection<AuthorDTO> authors;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Collection<AuthorDTO> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<AuthorDTO> authors) {
        this.authors = authors;
    }
}
