/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.Controllers;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ManagedBean;
import pl.sda.standalone.model.User;
import pl.sda.standalone.UserBeanInterface;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "userControler")
@RequestScoped
public class UserControler implements Serializable {

    @EJB
    private UserBeanInterface userBean;

    private String firstName;
    private String lastName;
    private String city;
    private String street;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    
    public List<User> getUsers() {
        return userBean.getUserList();
    }
    
    public void addUser() {
        User u1 = new User();
        u1.setFirstName(this.getFirstName());
        u1.setLastName(this.getLastName());
        u1.setStreet(this.getStreet());
        u1.setCity(this.getCity());
        userBean.addUser(u1);
    }
}
