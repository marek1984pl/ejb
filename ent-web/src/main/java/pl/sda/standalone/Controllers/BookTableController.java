/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.Controllers;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.modelmapper.ModelMapper;
import pl.sda.standalone.model.Book;
import pl.sda.standalone.BookBeanInterface;
import pl.sda.standalone.DTOs.AuthorDTO;
import pl.sda.standalone.DTOs.BookDTO;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "bookTableController")
@RequestScoped
public class BookTableController {

    @EJB
    private BookBeanInterface bookBean;

    public BookTableController() {
    }

    public List<Book> getBooks() {
        return bookBean.getListOfBooks();
    }
    
    public String getAuthors(BookDTO b) {
        
        String authors = "";
        for (AuthorDTO a : b.getAuthors()) {
            authors += a.getName() + ", ";
        } 
        return authors.substring(0, authors.length() - 2);
    }
    
    public List<BookDTO> getBooksDTO() {
        
        List<Book> books = bookBean.getListOfBooks();
        List<BookDTO> collection;
        collection = books.stream().map(this::mapBook).collect(Collectors.toList());
        
        return collection;
    }
    
    public BookDTO mapBook(Book b) {
        
        ModelMapper mapper = new ModelMapper();
        BookDTO mappedBook = mapper.map(b, BookDTO.class);
        
        return mappedBook;
    }
}
