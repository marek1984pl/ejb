/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.Controllers;

import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import pl.sda.standalone.DTOs.BookDTO;
import pl.sda.standalone.RentBeanLocal;
import pl.sda.standalone.model.Book;
import pl.sda.standalone.model.User;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "rentControler")
@SessionScoped
public class RentControler implements Serializable {

    @EJB
    private RentBeanLocal rentBean;
    
    @ManagedProperty (value = "#{loggedUser}")
    private LoginController loggedUser;
    
    private List<BookDTO> listOfRentedBooks = new ArrayList<>();
    
    public RentControler() {
    }
    
    public void rentBook(BookDTO b) {
        this.listOfRentedBooks.add(b);
    }

    public List<BookDTO> getListOfRentedBooks() {
        return listOfRentedBooks;
    }

    public void setListOfRentedBooks(List<BookDTO> listOfRentedBooks) {
        this.listOfRentedBooks = listOfRentedBooks;
    }
    
    public String calculatePayment() {
        return Integer.toString(this.listOfRentedBooks.size() * 10);
    }
    
    public void cancel(BookDTO b) {
        this.listOfRentedBooks.remove(b);
    }
    
    public void confirmRent() {
        List<Integer> book_ids = new ArrayList<>();

        for (BookDTO b : listOfRentedBooks)
            book_ids.add(b.getId());
        
        User user = new User();
        user.setId(2);
        
        rentBean.rentBooks(book_ids, user);
        listOfRentedBooks.clear();
    }

    public LoginController getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(LoginController loggedUser) {
        this.loggedUser = loggedUser;
    }
    
    public List<Book> getPreviouslyRentedBooks(User user) {
        return rentBean.getListOfPreviouslyRentedBooks(user);
    }
}
