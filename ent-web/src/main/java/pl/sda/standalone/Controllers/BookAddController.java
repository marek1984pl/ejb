/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.Controllers;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import pl.sda.standalone.model.Book;
import pl.sda.standalone.BookBeanInterface;
import pl.sda.standalone.model.Author;

/**
 *
 * @author marecki
 */
@ManagedBean(name = "bookAddController")
@RequestScoped
public class BookAddController {

    private String name;
    private String authors;
    private int isbn;
    private int year;
    
    @EJB
    private BookBeanInterface bookBean;
    
    public BookAddController() {
    }
    
    public void addBook() {
        Book book = new Book();
        book.setName(name);
        book.setIsbn(isbn);
        book.setYear(year);
        
        String [] authorNames = authors.split(",");
        ArrayList<Author> booksAuthors = new ArrayList<>();
        
        for (String name : authorNames) {
            Author author = new Author();
            author.setName(name);
            ArrayList<Book> booksList = new ArrayList<Book>();
            booksList.add(book);
            author.setBooks(booksList);
            booksAuthors.add(author);
        }
        
        book.setAuthors(booksAuthors);
        bookBean.addBook(book);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }
}
