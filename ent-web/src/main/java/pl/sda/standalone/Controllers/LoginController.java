/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.Controllers;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import pl.sda.standalone.UserBeanInterface;
import pl.sda.standalone.model.User;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    private String userLogin;
    private String password;
    private boolean userLogged;
    private User user;

    @EJB
    private UserBeanInterface userBean;

    public LoginController() {
        userLogged = false;
    }

    public void login() {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        if (session.isNew()) {
            session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        }

        user = userBean.logIn(this.getUserLogin(), this.getPassword());

        if (user != null) {
            this.setUserLogged(true);
            if (user.getUser_role() == 1) {
                session.setAttribute("userRole", "admin");
            } else {
                session.setAttribute("userRole", "user");
            }
        }
    }

    public void logout() {
        this.setUserLogged(false);
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        session.setAttribute("isLogged", "false");
        session.invalidate();
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isUserLogged() {
        return userLogged;
    }

    public void setUserLogged(boolean userLogged) {
        this.userLogged = userLogged;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserBeanInterface getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBeanInterface userBean) {
        this.userBean = userBean;
    }
}
