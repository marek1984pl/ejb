/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.sda.standalone.model.Author;
import pl.sda.standalone.model.Book;


@Stateless
public class BookBean implements BookBeanInterface{
    
    @PersistenceContext (unitName = "pu")
    public EntityManager em;

    public BookBean() {
    }
    
    @Override
    public List<Book> getListOfBooks() {
        Query query = em.createQuery("from Book b");
        return query.getResultList();
    }
    
    @Override
    public void addBook(Book b) {
        em.persist(b);
    }

    @Override
    public void addAuthor(Author author) {
        em.persist(author);
    }
}
