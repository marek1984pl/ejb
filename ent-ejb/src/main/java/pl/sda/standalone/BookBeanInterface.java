/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.standalone.model.Author;
import pl.sda.standalone.model.Book;

@Remote
@Local
public interface BookBeanInterface {

    public List<Book> getListOfBooks();
    public void addBook(Book b);
    public void addAuthor(Author author);
}
