/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.standalone.model.Book;
import pl.sda.standalone.model.User;

/**
 *
 * @author marecki
 */
@Local
@Remote
public interface RentBeanLocal {
    
    public void rentBooks(List<Integer> book_ids, User user);
    public List<Book> getListOfPreviouslyRentedBooks(User user);
}
