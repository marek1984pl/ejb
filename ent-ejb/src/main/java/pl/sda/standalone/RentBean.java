/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.sda.standalone.model.Book;
import pl.sda.standalone.model.Rent;
import pl.sda.standalone.model.User;

/**
 *
 * @author marecki
 */
@Stateless
public class RentBean implements RentBeanLocal {

    @PersistenceContext(unitName = "pu")
    public EntityManager em;

    @Override
    public void rentBooks(List<Integer> book_ids, User user) {
        
        Rent rent;
        
        // todo poprawic uzytkownika
        for (Integer id : book_ids) {
            rent = new Rent();
            rent.setBook(em.find(Book.class, id));
            rent.setUser(em.find(User.class, 2));
            rent.setRentalDate(new Date());
            rent.setReturnDate(null);
            em.persist(rent);
            em.flush();
        }
    }  

    @Override
    public List<Book> getListOfPreviouslyRentedBooks(User user) {
        // tutaj nie pobiera ksiazek tylko renty!!!
        Query query = em.createQuery("from Rent r where r.user = :user");
        //todo sprawdzic czy user czy user.getId()
        query.setParameter("user", user);

        List<Book> listOfBooks = query.getResultList();

        return listOfBooks;
    }
}
