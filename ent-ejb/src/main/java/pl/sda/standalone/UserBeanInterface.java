/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.standalone.model.User;

/**
 *
 * @author RENT
 */
@Local
@Remote
public interface UserBeanInterface {
    public void addUser(User user);
    public void editUser(User user);
    public List<User> getUserList();
    public User logIn(String userName, String password);
}
