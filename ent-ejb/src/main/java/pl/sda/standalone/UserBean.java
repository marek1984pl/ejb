/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.sda.standalone.model.User;

/**
 *
 * @author RENT
 */
@Stateless
public class UserBean implements UserBeanInterface {

    @PersistenceContext(unitName = "pu")
    public EntityManager em;

    public UserBean() {
    }
    
    @Override
    public void addUser(User user) {
        em.persist(user);
    }

    @Override
    public void editUser(User user) {
    }   

    @Override
    public List<User> getUserList() {
        Query query = em.createQuery("from User u");
        return query.getResultList();
    }
    
    @Override
    public User logIn(String username, String password) {
        Query query = em.createQuery("from User u where u.userLogin = :username and u.userPassword = :password");
        query.setParameter("username", username);
        query.setParameter("password", password);
        
        User user = (User) query.getSingleResult();
        
        return user;
    }
}
